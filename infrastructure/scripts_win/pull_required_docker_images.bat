@echo off

cd %~dp0

echo pulling mattgruter/artifactory
docker pull mattgruter/artifactory
echo.

echo pulling elleflorio/svn-server
docker pull elleflorio/svn-server
echo.

echo.
echo -----------------------------------------------------------------------------------
echo done.
pause