@echo off

cd %~dp0
echo starting from dir %CD%

echo.
echo setiing environment variables
call setenv_docker.bat

echo.
echo switching to %DOCKER_COMPOSE_DIR%
cd %DOCKER_COMPOSE_DIR%

echo.
echo starting artifactory
echo default user: admin/password
echo port: %ARTIFACTORY_CONATINER_HOSTPORT1%
docker-compose up -d artifactory
echo.

rem echo starting svn-server
rem echo default user: admin/password
rem docker-compose up svn_server 
echo.

echo.
echo =========================================================================
echo done.

pause