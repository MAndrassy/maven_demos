@echo off

cd %~dp0
echo starting from dir %CD%

echo setenv
call setenv_docker.bat

echo switching to %DOCKER_COMPOSE_DIR%
cd %DOCKER_COMPOSE_DIR%

echo stopping services
docker-compose down 


echo.
echo =========================================================================
echo done.

pause