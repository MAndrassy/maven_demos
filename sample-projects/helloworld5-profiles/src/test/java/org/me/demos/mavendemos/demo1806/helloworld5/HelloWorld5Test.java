package org.me.demos.mavendemos.demo1806.helloworld5;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.me.demos.mavendemos.demo1806.helloworld5.HelloWorld5;

public class HelloWorld5Test {

	@Test
	public void testInstantiateHelloWorldProgram() {
		
		HelloWorld5 dummy = new HelloWorld5();
		
		System.out.println("because we can - even with profiles");
		
		assertEquals("HelloWorld5", dummy.getClass().getSimpleName());		
	}
	
}
