package org.me.demos.mavendemos.demo1806.helloworld7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.me.demos.mavendemos.demo1806.helloworld7.HelloWorld7;

public class HelloWorld7Test {

	@Test
	public void testInstantiateHelloWorldProgram() {
		
		HelloWorld7 dummy = new HelloWorld7();
		
		System.out.println("because we can #7");
		
		assertEquals("HelloWorld7", dummy.getClass().getSimpleName());		
	}
	
}
