package org.me.demos.mavendemos.demo1806.helloworld3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.me.demos.mavendemos.demo1806.helloworld3.data.Person;

public class PersonTest {

	@Test
	public void testEqualityByName() {
		
		final Person person1 = new Person("Jack");
		final Person person2 = new Person("Jill");
		
		final Person person1b = new Person("Jack");
		
		assertEquals("equality by name failed (1)", person1, person1b);		
		assertNotEquals("equality by name failed (2)", person1, person2);		
	}
	
}
