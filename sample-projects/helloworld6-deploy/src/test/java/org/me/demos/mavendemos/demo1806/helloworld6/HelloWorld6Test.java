package org.me.demos.mavendemos.demo1806.helloworld6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HelloWorld6Test {

	@Test
	public void testInstantiateHelloWorldProgram() {
		
		HelloWorld6 dummy = new HelloWorld6();
		
		System.out.println("because we can #6");
		
		assertEquals("HelloWorld6", dummy.getClass().getSimpleName());		
	}
	
}
