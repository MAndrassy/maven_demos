package org.me.demos.mavendemos.demo1806.helloworld1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.me.demos.mavendemos.demo1806.helloworld4.HelloWorld4;

public class HelloWorld1Test {

	@Test
	public void testInstantiateHelloWorldProgram() {
		
		HelloWorld4 dummy = new HelloWorld4();
		
		System.out.println("brilliant - non-default test case dir works as well");
		
		assertEquals("HelloWorld4", dummy.getClass().getSimpleName());		
	}
	
}
