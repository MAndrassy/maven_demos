package org.me.demos.mavendemos.demo1806.helloworld2;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class HelloWorld2 {

	public static void main(String[] args) {

		(new HelloWorld2()).run();
	}

	public void run() {
		
		System.out.println("hello " + getPersonName() + "!");
	}
	
	public String getPersonName() {
		
	 try ( 
			InputStream is = this.getClass().getClassLoader().
				getResourceAsStream("org/me/short_package/person_name2.txt");
			Scanner scanner = new Scanner(is, StandardCharsets.UTF_8.name())
		) {
	        final String text = scanner.useDelimiter("(\\A|\n)").next();//.replaceAll("\n", "");
	        
	        return text;
	    } catch (IOException e) {

	    	throw new RuntimeException("Failed to load name-file", e); 
		}
	} //m
	
} //class
