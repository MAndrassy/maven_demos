package org.me.demos.mavendemos.demo1806.helloworld2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.me.demos.mavendemos.demo1806.helloworld2.HelloWorld2;

public class HelloWorld2Test {

	@Test
	public void testInstantiateHelloWorldProgram() {
		
		HelloWorld2 dummy = new HelloWorld2();
		
		System.out.println("because we can");
		
		assertEquals("HelloWorld2", dummy.getClass().getSimpleName());		
	}
	
	@Test
	public void testGetOtherNameFromTestResources() {
		
		final HelloWorld2 prog = new HelloWorld2();
		
		final String name = prog.getPersonName();
		
		System.out.println("Name found in test-data: '" + name + "'");
		
		assertEquals("Unexpected name", "Marilyn Monroe", name);
	}
	
}
