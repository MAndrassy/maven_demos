package org.me.demos.mavendemos.demo1806.helloworld1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HelloWorld1Test {

	@Test
	public void testInstantiateHelloWorldProgram() {
		
		HelloWorld1 dummy = new HelloWorld1();
		
		System.out.println("because we can");
		
		assertEquals("HelloWorld1", dummy.getClass().getSimpleName());		
	}
	
}
