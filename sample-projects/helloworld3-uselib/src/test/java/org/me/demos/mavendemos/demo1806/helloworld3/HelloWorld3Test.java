package org.me.demos.mavendemos.demo1806.helloworld3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.me.demos.mavendemos.demo1806.helloworld3.HelloWorld3;
import org.me.demos.mavendemos.demo1806.helloworld3.data.Person;

public class HelloWorld3Test {

	@Test
	public void testGetPerson() {
		
		HelloWorld3 dummy = new HelloWorld3();

		final Person person1 = dummy.getPerson();
		
		final Person person2 = new Person("Donald Knuth");
		
		assertEquals("Unexpected person", person2, person1);		
	}
	
}
