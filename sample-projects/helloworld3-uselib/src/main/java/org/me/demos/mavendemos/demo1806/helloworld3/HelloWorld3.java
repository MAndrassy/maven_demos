package org.me.demos.mavendemos.demo1806.helloworld3;

import org.me.demos.mavendemos.demo1806.helloworld3.data.Person;

public class HelloWorld3 {

	public static void main(String[] args) {

		(new HelloWorld3()).run();
	}

	public void run() {
		
		System.out.println("hello " + getPerson().getName() + "!");
	}
	
	public Person getPerson() {
		
		return new Person("Donald Knuth");
	}
}
